<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка");
?><h1>Доставка</h1>
 <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"personal",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "",
		"COMPONENT_TEMPLATE" => "personal",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(),
		"MENU_CACHE_TIME" => "3600000",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "about",
		"USE_EXT" => "N"
	)
);?>
<div class="global-block-container">
	<h2> <b>Ключевые особенности:</b> </h2>
	<p>
	</p>
	<ul>
		<li>Доставка нашими силами до адреса клиента: дома, подъезда или точке выдачи.</li>
		<li>Доставка ежедневно с понедельника по субботу согласно графика доставки*.<br>
 </li>
		<li>Доставка через терминал транспортных компаний ПЭК или Деловые линии, а также через главный автовокзал города.<br>
 </li>
		<li>Доставка абсолютно бесплатная.</li>
		<li>Доставка производиться по Ростову-на-Дону, а также большинству крупных населенных пунктов ростовской области и краснодарского края. Подробности в графике доставок*.</li>
	</ul>
	<p>
	</p>
	<p>
        &nbsp; &nbsp; &nbsp;<a target="_blank" href="/upload/dostavka/dostavka.pdf">График доставок</a><br>
 <br>
	</p>
	<h3> <b>Как сделать заказ на доставку?</b></h3>
	<p>
	</p>
	<ul>
		<li>Оформить заказ на сайте, выбрать желаемый способ и дату доставки. После этого менеджер свяжется с Вами для подтверждения заказа.</li>
		<li>Связаться с магазином напрямую по телефону или через Whats’up и согласовать заказ.<br>
 </li>
	</ul>
	<p>
	</p>
	<p>
		 &nbsp;
	</p>
 <b>Условия доставки:</b>
	<p>
	</p>
	<ul>
		<li>доставка осуществляется на следующий или другой день после дня оформления заказа.</li>
		<li>прием заказов на доставку ведется до 16:00.<br>
 </li>
		<li>минимальная сумма заказа – 1000 руб.<br>
		<br>
		<br>
		</li>
	</ul>
	<p>
	</p>
	<p>
	</p>
 <b>*Доставка осуществляется ежедневно</b><br>
	<h3><br>
 </h3>
	<div class="global-information-block">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "information_block",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
	</div>
</div>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>